/**
 * Watches ng projects dir for typescript changes
 * and compiles a full build of the npm package.
 */

const path = require('path');
const chokidar = require('chokidar');
const { spawn } = require('child_process');
const { isNullOrUndefined } = require('util');
const projects = path.resolve(__dirname, '..', 'lib', 'projects');
const ts = path.resolve(projects, '**', '*.ts');

const consumer = process.argv[2];

if (isNullOrUndefined(consumer)) {
  console.log('compost : you must enter an install path!');
  console.log('example: npm run dev ../a6lab');
}
try {
  // resolve the raw input into an os path
  consumerPath = path.resolve(consumer);
} catch (err) {
  console.log('compost : invalid install path, try again');
  process.exit(1);
}

console.log(`compost : watching ${ts} for changes...`);
console.log(`compost : will install to ${consumerPath}`);

/**
 * Install the created npm package into target project.
 * Assume the consumer project has a compost hook.
 */
const installPackage = () => {
  console.log('compost : package done, installing...');
  const installProcess = spawn(
    'npm',
    ['run', 'compost'],
    { cwd: consumerPath, stdio: 'inherit' },
  );
  installProcess.on('exit', () => {
    console.log('compost : package and install complete!');
  });
};

/**
 * Create the npm package. Once complete install.
 */
const handleFileChange = event => {
  console.log(`compost : file changed : ${event}`);
  const cwd = path.resolve(projects, '..');
  const packageProcess = spawn(
    'npm',
    ['run', 'package'],
    { cwd, stdio: 'inherit' }
  );
  packageProcess.on('exit', installPackage);
};

chokidar.watch(ts)
  .on('change', handleFileChange)
  .on('unlink', handleFileChange);

// do a build when first running
handleFileChange('boot');