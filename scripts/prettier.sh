#!/bin/bash

# run prettier on all projects
cd lib && \
  npm run prettier && \
  cd .. && \
  git add *.ts