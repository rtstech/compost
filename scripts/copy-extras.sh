#!/bin/bash

# Take a copy of the root config dir and move it to the dist dir
# so it is included in the npm package (tgz)

target_path="../lib/dist/compost-lib/"
cp -r -v -f ../config $target_path
cp -r -v -f ../style $target_path

echo 'compost : copied extras to dist/'