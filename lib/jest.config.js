module.exports = {
  preset: 'jest-preset-angular',
  globals: {
    'ts-jest': {
      tsConfigFile: 'src/tsconfig.spec.json'
    },
    '__TRANSFORM_HTML__': true,
  },
  setupTestFrameworkScriptFile: '<rootDir>/projects/compost-lib/src/setupJest.ts',
  transformIgnorePatterns: [
    'node_modules/(?!@ngrx)',
  ],
  moduleFileExtensions: [
    'ts',
    'js',
    'html',
    'json',
  ],
  collectCoverageFrom: [
    'projects/**/*.ts',
    '!**/modules/*.ts',
    '!**/(index|public_api|setupJest).ts',
    '!**/*.html',
  ],
  testResultsProcessor: 'jest-sonar-reporter',
  transform: {
    '^.+\\.(ts|tsx)$': '<rootDir>/node_modules/jest-preset-angular/preprocessor.js',
  },
};