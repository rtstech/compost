import { Injectable } from '@angular/core';
import { HttpClient, HttpParams } from '@angular/common/http';
import { Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { interval, Subject, Subscription } from 'rxjs';
import { first } from 'rxjs/operators';

const jwtHelper = new JwtHelperService();

@Injectable({
  providedIn: 'root',
})
export class AuthService {
  public logout$ = new Subject<undefined>();
  private reauth$ = Subscription.EMPTY;

  constructor(private http: HttpClient, public router: Router) {}

  get token(): string {
    return localStorage.getItem('token');
  }

  public isAuthenticated(): boolean {
    const token = this.token;
    if (token) {
      if (jwtHelper.isTokenExpired(token)) {
        return false;
      } else {
        this.watchToken();
        return true;
      }
    } else {
      this.logout();
      return false;
    }
  }

  private reauth() {
    const token = this.token;
    if (token) {
      return this.http
        .get('/reauth')
        .toPromise()
        .then((res: { token: string }) => {
          localStorage.setItem('token', res.token);
          this.watchToken();
          return true;
        })
        .catch(err => {
          this.logout();
          return false;
        });
    } else {
      this.logout();
    }
  }

  /**
   * Takes 50% off of the expiration and reauths.
   */
  public watchToken() {
    const now = new Date();
    const when = jwtHelper.getTokenExpirationDate(
      localStorage.getItem('token'),
    );
    const diff = (when.valueOf() - now.valueOf()) / 2;
    this.reauth$.unsubscribe();
    this.reauth$ = interval(diff)
      .pipe(first())
      .subscribe(() => this.reauth());
  }

  public login(user: string, pass: string, resource?: string) {
    let params = new HttpParams({ fromObject: { user, pass } });
    if (resource) {
      params = params.set('resource', resource);
    }
    return this.http
      .get('/auth', { params })
      .toPromise()
      .then((res: { token: string }) => {
        const decoded = jwtHelper.decodeToken(res.token);
        localStorage.setItem('token', res.token);
        localStorage.setItem('id', decoded.id);
        localStorage.setItem('level', decoded.security_level);
        localStorage.setItem('user', user);
        localStorage.setItem('resource', decoded.resource);
        console.log('auth service : decoded :', decoded);
        return true;
      })
      .catch(err => {
        console.log('auth : error :', err);
        return false;
      });
  }

  public logout(flushSession = false) {
    this.reauth$.unsubscribe();
    this.setReturnUrl();
    localStorage.removeItem('token');
    this.logout$.next();
    this.router.navigate(['/login']);
    if (flushSession) {
      // if (typeof sessionStorage !== 'undefined') {
      //   sessionStorage.clear();
      // }
      if (typeof localStorage !== 'undefined') {
        localStorage.clear();
      }
    }
  }

  private setReturnUrl() {
    if (typeof sessionStorage !== 'undefined') {
      const path = this.router['location'].path();
      sessionStorage.setItem('returnUrl', path);
      console.log('auth : set return url :', path);
    }
  }

  public getReturnUrl(): string | null {
    if (typeof sessionStorage !== 'undefined') {
      let cached = sessionStorage.getItem('returnUrl');
      cached = cached === '/login' ? null : cached;
      cached = cached === '/' ? null : cached;
      cached = cached === '/unauthorized' ? null : cached;
      console.log('auth : found return url :', cached);
      return cached;
    }
    return '';
  }
}
