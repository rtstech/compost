import { Injectable, isDevMode, Query } from '@angular/core';
import { Apollo, QueryRef } from 'apollo-angular';
import {
  WatchQueryOptions,
  OperationVariables,
  PureQueryOptions,
  ApolloQueryResult,
} from 'apollo-client';
import { Observable, interval } from 'rxjs';
import { first, tap, map } from 'rxjs/operators';
import { isNullOrUndefined } from 'util';

@Injectable({
  providedIn: 'root',
})
export class ApolloService {
  private watchedQueries = new Map<string, QueryRef<any>>();

  /**
   * Map first key edges in response from server.
   */
  static mapEdges(res): any[] {
    if (isNullOrUndefined(res.data) === false) {
      const k = Object.keys(res.data)[0];
      return res.data[k].edges;
    } else {
      return [];
    }
  }

  /**
   * Map first key edges in response from server.
   */
  static mapData(res): any {
    if (isNullOrUndefined(res.data) === false) {
      const k = Object.keys(res.data)[0];
      return res.data[k];
    } else {
      return {};
    }
  }

  constructor(public apollo: Apollo) {}

  /**
   * Return stored query by id.
   */
  public getWatchedQuery(queryId: string): QueryRef<any> {
    return this.watchedQueries.get(queryId);
  }

  /**
   * Store watched query in cache by id.
   */
  private setWatchedQuery(query: QueryRef<any>) {
    if (isDevMode()) {
      console.log(`apollo : set query : ${query.queryId}`);
    }
    this.watchedQueries.set(query.queryId, query);
  }

  /**
   * Delete a watched query cache record by id.
   */
  public deleteWatchedQuery(queryId: string) {
    this.watchedQueries.delete(queryId);
  }

  /**
   * Reset the watched query cache.
   */
  public flushWatchedQueries() {
    this.watchedQueries.clear();
  }

  /**
   * Wrapper for queries.
   */
  public query(
    query: WatchQueryOptions<OperationVariables>,
    once = true,
    watched = false,
  ): any {
    const take = once === true ? first() : tap();
    const watchedQuery = this.apollo.watchQuery(query);
    if (watched === true) {
      // this.setWatchedQuery(watchedQuery);
      return watchedQuery;
    } else {
      return watchedQuery.valueChanges.pipe(take);
    }
  }

  /**
   * A query mapped to the `mapData` resolver.
   */
  public queryMap(
    query: WatchQueryOptions<OperationVariables>,
    once?: boolean,
  ) {
    return (this.query(query, once, false) as Observable<{}>).pipe(
      map(ApolloService.mapData),
    );
  }

  /**
   * Wrapper for mutations.
   */
  public mutation(
    mutation: any,
    variables: { [key: string]: any },
    refetchQueries?: Array<string | PureQueryOptions>,
  ): Observable<any> {
    const model = { mutation, variables, refetchQueries };
    return this.apollo.mutate(model);
  }
}
