import { TestBed } from '@angular/core/testing';
import { ApolloModule } from 'apollo-angular';
import { ApolloService } from './apollo.service';

describe('ApolloService', () => {
  let service: ApolloService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ApolloModule],
      providers: [ApolloService],
    });
    service = TestBed.get(ApolloService);
  });

  it('should be created', () => {
    expect(service).toBeTruthy();
  });

  // @todo @brad
  xit('calls watch query on apollo', () => {});
});
