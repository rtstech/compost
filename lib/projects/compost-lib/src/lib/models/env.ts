export interface env {
  production: boolean;
  new_features: boolean;
  internal_use: boolean;
  server_uri: string;
  tableau_uri: string;
  google_key: string;
  code_version: string;
  auth_timeout?: number;
  version?: string;
  photoUploadQuality?: number;
}
