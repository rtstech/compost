export enum ActionStatus {
  default,
  loading,
  success,
  failure = -1,
}
