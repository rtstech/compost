import { Injectable, Inject } from '@angular/core';
import {
  HttpEvent,
  HttpInterceptor,
  HttpHandler,
  HttpRequest,
} from '@angular/common/http';
import { Observable } from 'rxjs';
import { env } from '../models/env';

/**
 * Prepends each request with the environment's server uri.
 */
@Injectable({
  providedIn: 'root',
})
export class APIInterceptor implements HttpInterceptor {
  constructor(@Inject('env') private env: env) {}
  intercept(
    req: HttpRequest<any>,
    next: HttpHandler,
  ): Observable<HttpEvent<any>> {
    const apiReq = req.clone({
      url: `${this.env.server_uri}${req.url}`,
    });
    return next.handle(apiReq);
  }
}
