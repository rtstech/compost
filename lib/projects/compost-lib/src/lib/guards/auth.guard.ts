import { Injectable } from '@angular/core';
import { CanActivate } from '@angular/router';
import { AuthService } from '../services/auth.service';

@Injectable()
export class AuthGuard implements CanActivate {
  constructor(private authService: AuthService) {}
  canActivate(): boolean {
    if (!this.authService.isAuthenticated()) {
      console.log('auth guard : not authenticated!');
      this.authService.logout();
      return false;
    }
    console.log('auth guard : authenticated!');
    return true;
  }
}
