import { FormGroup, Validators } from '@angular/forms';

export const passwordValidators = [
  Validators.required,
  Validators.minLength(8),
];

export const matchingPasswords = () => {
  return (group: FormGroup) => {
    const password = group.controls['password'];
    const confirmPassword = group.controls['confirmPassword'];
    const currentPassword = group.controls['currentPassword'];
    if (!password || !confirmPassword) {
      throw new Error(
        'invalid fields for matching password validator',
      );
    }
    if (password.value !== confirmPassword.value) {
      return {
        passwordMismatch: true,
      };
    }
    if (
      currentPassword &&
      currentPassword.value === confirmPassword.value
    ) {
      return {
        passwordSame: true,
      };
    }
  };
};
