import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { TableauComponent } from '../components/tableau/tableau.component';

const components = [TableauComponent];

// export { TableauComponent };

@NgModule({
  imports: [CommonModule],
  declarations: [...components],
  exports: [...components],
})
export class ComponentModule {}
