import {
  NgModule,
  ModuleWithProviders,
  Inject,
  PLATFORM_ID,
} from '@angular/core';
import { CommonModule, isPlatformBrowser } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { JWTInterceptor } from '../interceptors/jwt.interceptor';
import { APIInterceptor } from '../interceptors/api.interceptor';
import { ApolloService } from '../services/apollo.service';
import { AuthService } from '../services/auth.service';
import { ComponentModule } from './component.module';

const services = [ApolloService, AuthService];
const interceptors = [JWTInterceptor, APIInterceptor];
/**
 * Root module that exports all child modules.
 */
@NgModule({
  imports: [CommonModule, HttpClientModule, ComponentModule],
})
export class CompostLibModule {
  public static forRoot(environment): ModuleWithProviders {
    return {
      ngModule: CompostLibModule,
      providers: [
        ...services,
        ...interceptors,
        {
          provide: 'env',
          useValue: environment,
        },
      ],
    };
  }
  // constructor(@Inject(PLATFORM_ID) platformId: Object) {
  //   console.log('is browser?', isPlatformBrowser(platformId));
  // }
}
