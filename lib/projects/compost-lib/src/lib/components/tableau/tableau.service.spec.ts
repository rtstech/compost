import { TestBed, inject } from '@angular/core/testing';
import { HttpClientTestingModule } from '@angular/common/http/testing';
import { TableauService } from './tableau.service';

describe('TableauService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [HttpClientTestingModule],
      providers: [TableauService, { provide: 'env', useValue: {} }],
    });
  });

  it('should be created', inject(
    [TableauService],
    (service: TableauService) => {
      expect(service).toBeTruthy();
    },
  ));
});
