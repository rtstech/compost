import { ElementRef, Inject, Optional } from '@angular/core';
// import { DOCUMENT } from '@angular/common';
import { HttpClient } from '@angular/common/http';
import { Observable, BehaviorSubject, of } from 'rxjs';
import {
  map,
  catchError,
  take,
  filter,
  switchMapTo,
} from 'rxjs/operators';

import { env } from '../../models/env';

declare const tableau;

/**
 * This gets provided for every instance of the tableau component.
 */
export class TableauService {
  // is the tableau client-side js lib loaded?
  private hasApi = new BehaviorSubject<boolean>(false);
  private document: Document;

  constructor(
    private http: HttpClient,
    // @Optional() @Inject(DOCUMENT) private document: Document,
    @Inject('env') private env: env,
  ) {
    this.document = 'document' in window ? document : null;
    this.checkIfTableauExists();
  }

  /**
   * Loads a report by name.
   * Waits for the tableau js library to be available before getting token.
   */
  public getReport(
    element: ElementRef,
    report: string,
    config?: Object,
  ): any {
    return this.hasApi.pipe(
      filter(loaded => loaded === true),
      switchMapTo(this.getToken()),
      take(1),
      map(token => {
        let viz;
        const url = `${
          this.env.tableau_uri
        }/trusted/${token}/views/${report}`;
        const options = {
          ...config,
          hideTabs: true,
          hideToolbar: false,
          width: '100%',
          height: '100%',
          onFirstInteractive: () => {
            viz.refreshDataAsync();
          },
        };
        viz = new tableau.Viz(element.nativeElement, url, options);
        return viz;
      }),
    );
  }

  /**
   * If tableau hasn't been loaded, inject the script into the head.
   */
  private checkIfTableauExists() {
    if (!this.document) {
      console.warn('compost : tableau running without a document!');
      return;
    }
    if (window.hasOwnProperty('tableau') === false) {
      const script = this.document.createElement('script');
      script.src = `${
        this.env.tableau_uri
      }/javascripts/api/tableau-2.2.1.min.js`;
      script.async = true;
      script.defer = true;
      script.onload = () => this.hasApi.next(true);
      this.document.head.appendChild(script);
      console.log('tableau service : requesting script');
    } else {
      this.hasApi.next(true);
    }
  }

  /**
   * Loads a token which is required to obtain a report.
   */
  private getToken(): Observable<string> {
    const variables = {
      action: 'tableau_ticket',
    };
    const cacheBuster = Math.random()
      .toString(20)
      .substring(2);
    return (
      this.http
        // .get(`${this.env.server_uri}/rest`, { params: variables })
        .get(`/rest?c=${cacheBuster}`, { params: variables })
        .pipe(
          map((res: { data: string }) => {
            console.log('token ->', res.data);
            return res.data;
          }),
          catchError(() => {
            return of(null);
          }),
        )
    );
  }
}
