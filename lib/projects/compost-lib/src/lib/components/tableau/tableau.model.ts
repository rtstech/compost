export interface TableauVizPayload {
  viz: any;
  config: {
    [key: string]: any;
    handleSelection?: (a, b) => {};
  };
}

export interface TableauDataItem {
  value: string;
  formattedValue: string;
}
