import {
  Component,
  ChangeDetectionStrategy,
  ElementRef,
  EventEmitter,
  HostBinding,
  OnDestroy,
  OnInit,
  Input,
  ViewEncapsulation,
  ViewChild,
  Output,
} from '@angular/core';
import { TableauService } from './tableau.service';

@Component({
  selector: 'rts-tableau',
  templateUrl: './tableau.component.html',
  styleUrls: ['./tableau.component.scss'],
  encapsulation: ViewEncapsulation.None,
  changeDetection: ChangeDetectionStrategy.OnPush,
  providers: [TableauService],
})
export class TableauComponent implements OnInit, OnDestroy {
  // the report by id/name
  @Input()
  report: string;

  // and config options to extend defaults
  @Input()
  config: Object;

  // the insertion point in dom
  @ViewChild('tableauContainer')
  tableauContainer: ElementRef;

  @HostBinding('class.TableauContainer')
  true;

  // the tableau viz instance
  @Output()
  viz = new EventEmitter();

  constructor(private tableauService: TableauService) {}

  ngOnInit() {
    this.tableauService
      .getReport(this.tableauContainer, this.report, this.config)
      .subscribe(viz =>
        this.viz.emit({ viz, config: this.config }),
      );
  }

  /**
   * @todo @brad check to see if we need to do `viz.destroy()`
   */
  ngOnDestroy() {}
}
